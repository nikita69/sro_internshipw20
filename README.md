# General information about this git

I'm relatively new to git, but this page will mainly summarize the codes I wrote
during my Internship studying numerically Strontium Ruthenate with the help of 
the Abinit's package. Abinit's raw data files are kind of annoying if you want 
to export the data with python. I therefore wrote many codes which easily get
the proper data.

Note : these codes worked correctly with these versions of Abinit : 
8.10.3 and 9.0.2.

If you want to export these codes on your computer, you firstly need to get a 
ssh key on your machine. To get one, follow the instructions given [here](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair).

Once you have your key, [add](https://gitlab.com/profile/keys) it to your gitlab account.

Finally, you can just clone this project on your machine.

# More info. about this project
If you want to learn how to use a supercomputer and how to manage abinit's data,
I've made a jupyter notebook that you can visit [here](https://gitlab.com/nikita69/sro_internshipw20/-/blob/master/Abinit/Programming_howto_W20.ipynb).
If you want syntax highlighting in <i>.ipynb</i> under gitlab, you have to choose
[white syntax highlighting theme](https://gitlab.com/profile/preferences). 
