#!/usr/bin/python

import numpy as np
from shutil import copyfile

########################################################################################################################
class Lattice:
    '''
    This module treats Strontium Ruthenate's different sets of basis to change atomic coordinates (xred) with the
    appropriate basis. Can be useful to switch between conventionnal cell and unit-cell when applying stress. You can
    for instance build the input files for variable strains to do bands calculations or projectors.
    Last update :
    To do : add visualization of the lattice
    '''

    def __init__(self, path, cell, ndt=1):
        '''
        Init method to build the proper atomic datas in the proper basis.
        Atomic orders HAVE TO be : Ru - O - Sr. (to do : add possibility to switch atomic orders)
        :param path: input atomic datas coordinates file path. It has to be a file provided thanks to Abidata.make_relax
        :param cell: cell parameters (same format as type)
        :param ndt: number of datas.
        To do : add type='.out' with abidata.
        '''

        self.ndata = int(ndt)
        self.root = path

        #Generate the data file
        self.data = np.genfromtxt(self.root)
        self.cell = np.genfromtxt(cell, usecols=(1,2,3))

        #Checking shape of data
        if np.shape(self.data)[0] % 14 == 0 or np.shape(self.data)[0] % 7 == 0:
            pass
        else:
            raise ValueError('Number of atoms is incorrect. \n '
                             'Strontium ruthenate has 7 atoms within its unit-cell. \n'
                             'If you are working within the conventional cell, you must have 14 atoms.')

        if np.shape(self.data)[0] == self.ndata*7:
            pass
        elif np.shape(self.data)[0] == self.ndata*14:
            pass
        else:
            raise ValueError('Number of data is incorrect (default value is 1).')

        self.natoms = int(np.shape(self.data)[0]/self.ndata)

        print('Data loaded with correct dimensions.\n'
              'Be aware : only cartesian cell parameters are implemented (related to relaxations).')

    def reshape(self, initial, final, **kwargs):
        '''
        Reshape data with another atomic order.
        :param initial: dictionnary containing initial atomic order (for instance : {'Ru': 1,'O': 4, 'Sr': 2})
               Atoms are keys and values are list containing positions.
        :param final: string containing final atomic order with the same format as initial.
               Warning : it is important you keep the same atomic order as in the initial dictionnary.
        :return: atomic data with new order.
        '''
        #Testing dictionnaries
        liste = [initial, final]
        for elem in liste:
            if len(elem['Ru']) == 2 and self.natoms == 14 or len(elem['Ru']) == 1 and self.natoms == 7:
                pass
            elif len(elem['Sr']) == 4 and self.natoms == 14 or len(elem['Sr']) == 2 and self.natoms == 7:
                pass
            elif len(elem['O']) == 8 and self.natoms == 14 or len(elem['O']) == 4 and self.natoms == 7:
                pass
            else:
                raise ValueError('Here are the possible number of atoms : \n'
                                 'Ru : 1 or 2\n'
                                 'O : 4 or 8\n'
                                 'Sr : 2 or 4\n'
                                 'You did not provide a correct amount of atoms')

        self.new_data = np.empty(shape=np.shape(self.data), dtype=float)
        for j in range(self.ndata):
            for key in initial:
                for i in range(len(initial[key])):
                    new_pos = final[key][i]
                    old_pos = initial[key][i]
                    self.new_data[new_pos+self.natoms*j,:] = self.data[old_pos+self.natoms*j,:]

        name = kwargs.get('name', None)
        relax = kwargs.get('relax', False)
        self.write(name, relax)


    def change_basis(self, initial_basis, final_basis, **kwargs):
        '''
        Function to change the atomic coordinates within the new basis.
        :param initial_basis: 'inplane', 'octahedron', or 'cartesian'
        :param final_basis: 'inplane', 'octahedron', or 'cartesian'
        '''
        self.basei = initial_basis
        self.basef = final_basis
        #Build the linear transformation matrix
        #cartesian - octahedron
        if (initial_basis == 'cartesian' and final_basis == 'octahedron') or (
                initial_basis == 'octahedron' and final_basis == 'cartesian'):
            if np.shape(self.data)[0] != 14 * self.ndata:
                raise ValueError('Using cartesian basis requires 14 atoms and not 7.')
            TransfoMatrix = [[1, 1, 0], [0, 1, 1], [1, 0, 1]]
            if initial_basis == 'octahedron':
                TransfoMatrix = np.linalg.inv(TransfoMatrix)

        #inplane - cartesian
        elif (initial_basis == 'inplane' and final_basis == 'cartesian') or (
                initial_basis == 'cartesian' and final_basis == 'inplane'):
            if np.shape(self.data)[0] != 14 * self.ndata:
                raise ValueError('Using cartesian basis requires 14 atoms and not 7.')
            TransfoMatrix = [[1,0,1],[0,1,1],[0,0,2]]
            if initial_basis == 'inplane':
                TransfoMatrix = np.linalg.inv(TransfoMatrix)

        #octahedron - inplane
        elif (initial_basis == 'octahedron' and final_basis == 'inplane') or (
                initial_basis == 'inplane' and final_basis == 'octahedron'):
            if np.shape(self.data)[0] != 7*self.ndata:
                raise ValueError('Unit-cell basis require 7 atoms and not 14.')
            octa_to_carta = np.linalg.inv([[1, 1, 0], [0, 1, 1], [1, 0, 1]])
            carta_to_in = [[1,0,1],[0,1,1],[0,0,2]]
            TransfoMatrix = np.dot(carta_to_in, octa_to_carta)
            if initial_basis == 'inplane':
                TransfoMatrix = np.linalg.inv(TransfoMatrix)

        else:
            raise NotImplementedError('Basis not implemented')

        self.new_data = np.empty(shape=np.shape(self.data))
        for i in range(np.shape(self.data)[0]):
            self.new_data[i,:] = np.dot(TransfoMatrix, np.transpose(self.data[i,:]))

        name = kwargs.get('name', None)
        relax = kwargs.get('relax', True)
        self.write(name, relax)


    def write(self, name=None, relax=True):
        '''
        Function to write the new coordinates with respect to the new basis
        :param name: where to write the new data (file root name will be the same as input file)
        :param relax: checks if the basis change is related to relaxation computations
        '''

        self.write_file = name
        if name==None:
            self.write_file='new_'+self.root
        newpath = self.write_file
        with open(newpath, 'w') as f:
            #Writing new data
            for i in range(self.ndata):
                f.write('#%d\n' %i)
                if self.natoms == 7:
                    for j in range(7):
                        for k in range(3):
                            f.write('%.15f ' %(self.new_data[j+7*i,k]))
                            if k==2:
                                f.write('\n')
                elif self.natoms == 14 and relax==True:
                    for j in [0,2,3,4,5,10,11]:
                        #order is Ru2O8Sr4 here
                        for k in range(3):
                            f.write('%.15f ' % (self.new_data[j+14*i, k]))
                            if k == 2:
                                f.write('\n')
                else:
                    for j in range(14):
                        for k in range(3):
                            f.write('%.15f ' % (self.new_data[j+14*i, k]))
                            if k == 2:
                                f.write('\n')
        print('New data written in %s' %self.write_file)


    def make_input(self, input, format='multidata', name=None):
        '''
        Function to make input files with the corrected basis and atomic coordinates.
        :param input: Abinit input (vanilla) file. By vanilla, I mean that all the parameters different from cell/xred
                      must be provided in the file.
        :param format: if self.ndata > 1, 'multidata' or 'separated' since sometimes abinit has problems with ndtset>10
        To do : check if cell parameters/xred are already present in the file and delete them.
                add modification ndata=1
        '''
        if format=='separated' and isinstance(name,list)==False:
            raise TypeError('You need to provide a list of names with this format.')
        elif format=='separated' and len(name) != self.ndata:
            raise TypeError('The number of output files you provided is not equal to the number of data')
        else:
            pass
        if name == None:
            raise TypeError('Please provide an output name')
        else:
            pass

        if format == 'multidata':
            copyfile(input, name)
            abifile = name
            with open(abifile, 'a') as f:
                f.write('\nndtset %d\n\n' %(self.ndata))
                if self.basef == 'inplane':
                    for i in range(self.ndata):
                        i_n = i+1
                        f.write('rprim%d  ' %i_n)
                        f.write('%2.15f 0 0\n' %(self.cell[i,0]))
                        f.write('0 %2.15f 0\n' %(self.cell[i,1]))
                        f.write('%2.15f %2.15f %2.15f\n' %(-self.cell[i,0]/2, -self.cell[i,1]/2, self.cell[i,2]/2))
                elif self.basef == 'octahedron':
                    for i in range(self.ndata):
                        i_n = i+1
                        f.write('rprim%d  ' % i_n)
                        f.write('%2.15f %2.15f %2.15f\n' % (self.cell[i, 0]/2, self.cell[i,1]/2, -self.cell[i,2]/2))
                        f.write('%2.15f %2.15f %2.15f\n' % (-self.cell[i, 0]/2, self.cell[i,1]/2, self.cell[i,2]/2))
                        f.write('%2.15f %2.15f %2.15f\n' % (self.cell[i, 0]/2, -self.cell[i,1]/2, self.cell[i,2]/2))
                f.write('\n')
                n_f  = open(self.write_file, 'r')
                lines = n_f.readlines()
                for i in range(self.ndata):
                    i_n = i+1
                    f.write('xred%d  ' %i_n)
                    for line in lines[8*i+1:8*(i+1)]:
                        f.write(line)
                n_f.close()
        elif format=='separated':
            n_f = open(self.write_file, 'r')
            lines = n_f.readlines()
            for i in range(self.ndata):
                i_n = i+1
                copyfile(input, name[i])
                abifile = name[i]
                with open(abifile, 'a') as f:
                    if self.basef == 'inplane':
                        f.write('\nrprim  ')
                        f.write('%2.15f 0 0\n' % (self.cell[i, 0]))
                        f.write('0 %2.15f 0\n' % (self.cell[i, 1]))
                        f.write('%2.15f %2.15f %2.15f\n' % (
                        -self.cell[i, 0] / 2, -self.cell[i, 1] / 2, self.cell[i, 2] / 2))
                    elif self.basef == 'octahedron':
                        f.write('rprim  ')
                        if self.ndata == 1:
                            f.write('%2.15f %2.15f %2.15f\n' % (
                                self.cell[0] / 2, self.cell[1] / 2, -self.cell[2] / 2))
                            f.write('%2.15f %2.15f %2.15f\n' % (
                                -self.cell[0] / 2, self.cell[1] / 2, self.cell[2] / 2))
                            f.write('%2.15f %2.15f %2.15f\n' % (
                                self.cell[0] / 2, -self.cell[1] / 2, self.cell[2] / 2))
                        else:
                            f.write('%2.15f %2.15f %2.15f\n' % (
                            self.cell[i, 0] / 2, self.cell[i, 1] / 2, -self.cell[i, 2] / 2))
                            f.write('%2.15f %2.15f %2.15f\n' % (
                            -self.cell[i, 0] / 2, self.cell[i, 1] / 2, self.cell[i, 2] / 2))
                            f.write('%2.15f %2.15f %2.15f\n' % (
                            self.cell[i, 0] / 2, -self.cell[i, 1] / 2, self.cell[i, 2] / 2))
                    f.write('\n')
                    f.write('xred  ')
                    for line in lines[8*i+1:8*(i+1)]:
                        f.write(line)
            n_f.close()

        print('Input file updated with cell parameters and atomic datas')
