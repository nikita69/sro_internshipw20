#!/usr/bin/python

import numpy as np
import re
Ha_to_eV = 27.211396132
########################################################################################################################
class Abidata:
    '''
    This module treats Abinit's different type of output to get the proper data.
    Note : relaxation calculations are only implemented with SRO (or any other material which conventional cell has 14
           atoms).
    Can be useful to get data for band-structure/dos or even kpt to build a mesh (Olivier Gingras' codes).
    Last update :
    '''

    def __init__(self, file, type, **kwargs):
        '''
        Initialize the data type.
        :param file: Path for the abinit file.
               Note : for fatbands and relax you have to provide a list of strings.
        :param type: Type of the file ('dos', 'band', 'fat', 'relax', 'kpt').
                     'dos' is any abinit's _DOS file but with
                     either prtdos=1 or prtdos=3 or prtdos=3+prtdosm=1.
                     'band' is any abinit's _EIG file.
                     'fat' is any abinit's _FATBANDS file.
                     'relax' is any abinit's .out file following a relaxation calculation.
                     'kpt' is any abinit's .out file made with prtvol=3 or prtvol=4.
        '''

        self.type = type
        self.root = file
        self.ndata = kwargs.get('ndata', None)
        self.fermi = kwargs.get('fermi', None)
        self.name = kwargs.get('name', None)
        if self.name == None and self.type == 'relax':
            name = ['data_cell', 'data_xred']
        elif self.name == None and self.type == 'kpt':
            name = self.root+'.klist'
        else:
            pass
        if self.fermi == None and (self.type=='band'):
            raise ValueError('You have to provide the fermi energy for fatbands/bands calculations (in eV)')
        else:
            pass
        #Test path
        #with open(self.root, 'r'):
         #   pass

        if type == 'fat' and len(self.root) == 1:
            raise TypeError('Type == "fat" requires an additional file. \n'
                           'Please provide a file containing kpts with "file_k = ...".\n'
                           'Type "help(Abidata.make_fat) for more information.')
        elif type == 'relax' and self.ndata == None:
            raise TypeError('Type == "relax" requires to provide the number of datasets in the .out file. \n'
                            'Please provide this with "ndata = ...".')
        elif type == 'relax' and 10 < self.ndata < 18 and len(self.root) < 2:
            raise TypeError('You have to provide two files with 10 < ndata < 18.')
        elif type == 'relax' and self.ndata > 18:
            raise TypeError('Maximum number of ndata implemented is 18.')
        elif type not in ['dos', 'band', 'fat', 'relax', 'kpt']:
            raise NotImplementedError('This type of file is not implemented. Please choose between the following :\n'
                                      '"dos", "band", "fat", "relax" or "kpt".\n'
                                      'Type "help(Abidata.__init__)" for more info.')

    def make(self):
        '''
        Get the data from root file.
        :return: data in a plottable shape.
        To do : add a check on whether the file is _EIG, _DOS, _FATBANDS or .out.
        '''

        if self.type == 'band':
            return self.make_band(self.root, self.fermi)
        elif self.type == 'dos':
            return self.make_dos(self.root)
        elif self.type == 'fat':
            return self.make_fat(self.root)
        elif self.type == 'relax':
            self.make_relax(self.root, self.ndata, self.name)
        else:
            self.make_kpt(self.root, self.name)

    def make_band(self, file, fermi):
        '''
        Get bands data from band calculations.
        :param file: Abinit's _EIG file.
        :return: path kpts and energy eigenvalues on each kpt.
        To do : select version and correct for latest version
        '''
        #You must provide an _EIG file
        bands = open(file, 'r')
        lines = bands.readlines()
        #Globals
        print(lines[0].split()[6])
        fermi = float(lines[0].split()[6])
        for i, elem in enumerate(lines[1].split()):
            if 'nkpt=' == elem:
                i_kpt = i+1
                break
        for i, elem in enumerate(lines[2].split()):
            if 'nband=' == elem:
                i_band = i+1
                break
        self.nkpt = int(lines[1].split()[i_kpt])
        if '(hartree)' in lines[0].split():
            scaling = Ha_to_eV
        elif 'eV' in lines[0].split():
            scaling = 1
        fermi *= scaling
        self.nband = int(lines[2].split()[i_band][:-1])
        nline_at_kpt = int(self.nband/8)
        if self.nband % 8 != 0:
            nline_at_kpt += 1
        else:
            pass
        #Getting the data
        data = np.empty(shape=(self.nkpt, self.nband), dtype=float)
        kpts = []
        for i in range(self.nkpt):
            local_data = lines[2 + (nline_at_kpt+1) * i:2 + (nline_at_kpt+1) * (i + 1)]
            line_k = local_data[0].split()
            kpts.append([float(line_k[7]), float(line_k[8]), float(line_k[9])])
            i_line = 1
            for j in range(self.nband):
                line = local_data[i_line].split()
                data[i,j] = float(line[j-(i_line-1)*8])*scaling
                data[i,j] -= fermi
                if (j+1)%8==0:
                    i_line +=1
        bands.close()

        return data, kpts

    def make_dos(self, file):
        '''
        Get dos data from dos calculations.
        :param file: Abinit _DOS file with either prtdos=1, prtdos=3 or prtdos=3+prtdosm=1
        :return: For each type of DOS, energy data is provided. For prtdos=1, dictionnary with keys 'dos' and 'intdos'
                 For prtdos=3, same. For prtdos=3+prtdosm=1, dictionnary with the following keys :
                 loc i, integ i, i j, where i,j denote orbital indexing (l,m).
        To do : add other smearing for first dostype
        '''
        #You must provide a _DOS file
        #Note : prtdosm=1+prtdos3 give 2 type of files : projected orbitals and a file like prtdos=3.
        dos = open(self.root, 'r')
        lines = dos.readlines()
        self.fermi = float(lines[7].split()[4])  #Hartree
        self.fermi *= Ha_to_eV
        #check the type of dos file
        for i, line in enumerate(lines):
            if '(tsmear/2)' in line.split():
                dos_type = 1
                break
            elif 'l=0' in line.split():
                dos_type = 31
                break
            elif len(line.split()) == 3 and i>20:
                dos_type = 3
                break
            elif i>20:
                raise NotImplementedError('Only prtdos=1 and prtdos=3 (+prtdosm=1) are implemented')
        if dos_type == 1 or dos_type == 3:
            data_dos = {'dos': [], 'intdos': []}
            data_energy = []
            if dos_type == 1:
                local_data = lines[16:]
            else:
                local_data = lines[15:]
            for line in local_data:
                line = line.split()
                data_energy.append(float(line[0])*Ha_to_eV)
                data_dos['dos'].append(float(line[1]))
                data_dos['intdos'].append(float(line[2]))

        elif dos_type == 31:
            n_orb = 5
            l_v = [i for i in range(n_orb)]
            conn = ['loc', 'integ']
            l_b = [[str(conn[j])+' '+str(i) for i in l_v] for j in range(2)]
            m = [[-i+j for j in range(2*i+1)] for i in l_v]
            lm = [[str(l_v[i])+' '+str(m[i][j]) for j in range(len(m[i]))] for i in range(len(l_v))]
            l_tot = l_b + lm
            l_tot_sp = []
            #Reshaping the l with a single list
            for elem in l_tot:
                for i in elem:
                    l_tot_sp.append(i)
            data_dos = {i: [] for i in l_tot_sp}
            data_energy = []
            local_data = lines[19:]

            for line in local_data:
                line = line.split()
                data_energy.append(float(line[0])*Ha_to_eV)
                for i, elem in enumerate(data_dos):
                    i_n = i+1
                    data_dos[elem].append(float(line[i_n]))

        dos.close()
        return data_energy, self.fermi, data_dos

    def make_fat(self, file):
        '''
        Get fatbands from abinit's fatbands calculations.
        :param file: list of abinit _FATBANDS file and _EIG file for the kpts.
        :return: Dictionnary with the following keys :
        To do : add .out files for file_k with prtvol=3 or 4.
        '''
        fatbands = open(file[0], 'r')
        lines = fatbands.readlines()
        useless_energy, kpts = self.make_band(file[1], 0)
        data_fat = np.empty(shape=(self.nkpt, self.nband), dtype=float)
        for i, line in enumerate(lines):
            if '@type' in line.split():
                i_line = i+3
                break
            else:
                pass
        data_min = np.empty(shape=(self.nkpt,self.nband), dtype=float)
        data_max = np.empty(shape=(self.nkpt, self.nband), dtype=float)
        data_energy = np.empty(shape=(self.nkpt, self.nband), dtype=float)
        for i in range(self.nband):
            local_data = lines[2*i+i_line + self.nkpt * i:i_line + self.nkpt * (i + 1)+2*i]
            for j in range(self.nkpt):
                data_energy[j,i] = float(local_data[j].split()[1])
                data_fat[j,i] = float(local_data[j].split()[2])
                data_min[j,i] = data_energy[j,i] - data_fat[j,i]
                data_max[j,i] = data_energy[j,i] + data_fat[j,i]
        fatbands.close()

        return data_energy, data_min, data_max, kpts

    def make_relax(self, file, ndata, name):
        '''
        Get relaxation cell parameters from abinit .out file.
        :param file : LIST of strings containing all the datas. Note
        :param ndata : ndtset in relaxation calculation. Normally this value can be found in the .out file but since
               abinit seems to glitch when using ndtset>10, this parameter is useful to combine results into a single
               file. Maximum number of data implemented yet is 18.
        :return: Cell and xred parameters in a format usable by lattice.py.
        '''
        def chk_0(nb):
            if np.abs(nb) < 0.0000001:
                nb = 0
            else:
                pass
            return nb

        file_o = []
        lines = []
        for i in range(len(file)):
            file_o.append(open(file[i], 'r'))
            lines.append(file_o[i].readlines())
        for i, elem in enumerate(lines):
            for j, line in enumerate(elem):

                if 'DATASET(S)' in line.split():
                    end_ind = j
                    break
            lines[i] = lines[i][end_ind:]
        self.cell = []
        self.xred = []
        for i in range(ndata):
            chk_cell = len(self.cell)
            chk_red = len(self.xred)
            if i>8:
                ind_file = 1
                i_n = i-8
            else:
                ind_file = 0
                i_n = i+1
            for j, line in enumerate(lines[ind_file]):
                line = line.split()
                if 'acell'+str(i_n) in line:
                    self.cell.append([float(line[1]), float(line[2]), float(line[3])])
                elif 'xred'+str(i_n) in line:
                    for k in range(14):
                        if k==0:
                            inds = [1,2,3]
                        else:
                            inds = [0,1,2]
                        self.xred.append([chk_0(float(lines[ind_file][j+k].split()[inds[0]])),
                                          chk_0(float(lines[ind_file][j+k].split()[inds[1]])),
                                          chk_0(float(lines[ind_file][j+k].split()[inds[2]]))])

                else:
                    pass
                if len(self.cell) == chk_cell + 1 and len(self.xred) == chk_red + 1:
                    break
                else:
                    pass
        for i in file_o:
            i.close()

        with open(name[0], 'w') as f:
            for i in range(ndata):
                f.write('acell'+str(i)+'\t%2.15f\t%2.15f\t%2.15f\tBohr\n' %(self.cell[i][0], self.cell[i][1],
                                                                            self.cell[i][2]))
        with open(name[1], 'w') as f:
            for i in range(ndata*14):
                if i % 14 == 0:
                    f.write('#'+str(int(i/14))+'\n')
                f.write('%1.15f\t%1.15f\t%1.15f\n' %(self.xred[i][0], self.xred[i][1], self.xred[i][2]))


    def make_kpt(self, file, name):
        '''
        Get all the kpts from an abinit's calculations made with prtvol=3 or 4.
        :param file: .out file made with prtvol=3 or 4.
        :return: klist written in a file.
        '''
        out = open(file, 'r')
        lines = out.readlines()
        for i, line in enumerate(lines):
            for j, elem in enumerate(line.split()):
                if elem == 'nkpt':
                    self.nkpt = int(line.split()[j+2])
                    break
            if 'kpt' in line.split():
                ind_k = i
                break
        kpts = lines[ind_k:ind_k+self.nkpt]
        self.kpts = []
        for i, line in enumerate(kpts):
            line = line.split()
            if i==0:
                ind = [1,2,3]
            else:
                ind = [0,1,2]
            self.kpts.append([float(line[ind[0]]), float(line[ind[1]]), float(line[ind[2]])])
        out.close()

        with open(name, 'w') as f:
            f.write('%d\n' %self.nkpt)
            for i in range(self.nkpt):
                f.write('%1.15f %1.15f %1.15f\n' %(self.kpts[i][0], self.kpts[i][1], self.kpts[i][2]))

