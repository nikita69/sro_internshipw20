#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.optimize import curve_fit
from shutil import copyfile

########################################################################################################################
class Convergence:
    '''
    This module treats Strontium Ruthenate's convergence studies regarding kpts, ecut and pawecutdg.
    The convergence studies are plotted. The convergence parameters are not returned because I prefer you to judge
    yourself from their values with the plots.
    Can be useful to build various datasets with a clean vanilla file and therefore reduce the probability of getting
    errors. Moreover, sometimes abinit is kind of limited to build datasets that follow a certain pattern, python will
    easily handle this.
    Even if it is possible to add many parameters, I recommend to use only 1 parameter since the data you obtain
    from one parameter can be (actually should be) used for the other parameters.
    The convergence studies will better work with a unit-cell; with a conventionnal cell, it is possible the smoothing
    crashes. With another material than SRO the smoothing won't work.
    Last update :
    To do :
    '''

    def __init__(self, file, params):
        '''
        Initialize the convergence study by providing a vanilla file and
        :param file: Vanilla abinit input file which whill get modified.
        :param params: params to do a convergence study on and their values. Format is a dictionnary.
               For instance : params={'ngkpt':[[10,10,10],[20,20,20]], 'ecut': [[5],[10],[15]]}.
               Defining 'kptrlatt' follow this pattern : 'kptrlatt':[[1,0,0,0,1,0,0,0,1],...]
               where it means kptrlatt 1 0 0\n 0 1 0\n 0 0 1
               Note : it is important that the values are list of lists !
        '''

        self.vanilla = file
        #chk path
        with open(self.vanilla) as f:
            pass
        self.params = params

        #Check if the vanilla file already has these variables in it
        with open(self.vanilla) as f:
            lines = f.readlines()
            for line in lines:
                line = line.split()
                #chk if the file is already multidataset
                if 'ndtset' in line:
                    raise ValueError('Your vanilla file is already multidataset. Please change it to a single dataset')
                else:
                    pass
                for param in params:
                    if param in line:
                        raise ValueError('The parameter %s you are about to do a convergence study on is already present'
                                         ' in this file. Please remove it.' %param)
                    elif param=='ngkpt' and 'kptrlatt' in line or param=='kptrlatt' and 'ngkpt' in line:
                        raise ValueError('You used either "ngkpt" or "kptrlatt" as a convergence parameter while the '
                                         'other one is present in the file.')
                    else:
                        pass
    def make_inputs(self, format, write):
        '''
        Make inputs file that you will be able to execute with abinit.
        :param format: Choose between 'multidata' and 'separated'. The first uses abinit's 'ndtset' and the second
               separates the parameters values between different files.
               Note : I only recommend "separated" with kpts parameters since these parameters have an influence on
               the parallelisation. You should build kpts convergence studies separately from other parameters.
        :param write: says whether on not you write the new files. The usefulness of this variable is to keep
               self.format even after you already created the new files. If you use python with ipython, or jupyter;
               this variable is clearly not useful.
        :return: write new abinit input files
        '''
        self.format = format
        if self.format not in ['multidata', 'separated']:
            raise ValueError('Please provide one of the following formats : \n'
                             '"multidata" or "separated"')
        if write==True:
            if self.format == 'multidata':
                for key in self.params:
                    newfile = self.vanilla+'_'+key
                    copyfile(self.vanilla, newfile)
                    with open (newfile, 'a') as f:
                        f.write('\n')
                        f.write('ndtset %d\n' %len(self.params[key]))
                        for i, param in enumerate(self.params[key]):
                            i_n = i + 1
                            f.write(key + str(i_n) + ' ')
                            for j, elem in enumerate(param):
                                if j==3 or j==6:
                                    f.write('\n')
                                f.write(str(elem)+' ')
                            f.write('\n')

            else:
                for key in self.params:
                    for i, param in enumerate(self.params[key]):
                        i_n = i+1
                        newfile = self.vanilla + '_' + key + str(i_n)
                        copyfile(self.vanilla, newfile)
                        with open(newfile, 'a') as f:
                            f.write('\n')
                            f.write(key + ' ')
                            for j, elem in enumerate(param):
                                if j == 3 or j == 6:
                                    f.write('\n')
                                f.write(str(elem)+' ')

    def get_data(self, files):
        '''
        Get the convergence data after abinit calculations.
        :param files: dictionnary of files where to get the data. It is important that files are list of files.
               For instance, if you had self.params = {'ecut':[[10],[20]]} and self.format='separated'; files take this format:
               {'ecut':['path/to/file_10', 'path/to/file_20']}
        :return: dictionnary of param values with parameters as keys.
        Note : format = 'separated' has not really been tested.
        To do : add other criterions than 'etotal'.
        '''
        #Check if you provided the correct amount of files.
        for key in self.params:
            if key not in files:
                raise ValueError('You did not provide .out files for the parameter %s' %self.params[key])
            elif len(files[key]) != len(self.params[key]) and self.format == 'separated':
                raise ValueError('You did not provide the correct amount of files for the parameter %s' %self.params[key])

        #Check if the .out file contains the correct input parameters
        '''if self.format == 'separated':
            for key in self.params:
                for i in range(len(self.params[key])):
                    with open(files[key][i], 'r') as f:
                        lines = f.readlines()
                        for line in lines:
                            line = line.split()
                            if key in line and self.params[key][i] not in line:
                                raise ValueError('The value of %s is not correct in the file %s' %(key, files[key][i]))
                            elif key not in line:
                                raise ValueError('File %s does not contain the parameter %s' %(files[key][i], key))
                            elif key in line and self.params[key][i] in line:
                                pass'''
        #Does not work yet
        #add the check if self.format == multidata

        #Get data
        self.etotal = [[] for i in range(len(self.params))]
        if self.format == 'multidata':
            for j, key in enumerate(self.params):
                with open(files[key][0], 'r') as f:
                    lines = f.readlines()
                    for line in lines:
                        line = line.split()
                        for k, elem in enumerate(line):
                            for i in range(len(self.params[key])):
                                i_n = i+1
                                if 'etotal'+str(i_n) == elem:
                                    self.etotal[j].append(float(line[k+1]))
                                else:
                                    pass
        elif self.format == 'separated':
            bool_test = False
            for i, key in enumerate(self.params):
                for j in range(len(self.params[key])):
                    with open(files[key][j], 'r') as f:
                        lines = f.readlines()
                        for line in lines:
                            line = line.split()
                            for k,elem in enumerate(line):
                                if 'etotal' == elem:
                                    self.etotal[i].append(float(line[k+1]))
                                    break
                                    bool_test = True
                            if bool_test == True:
                                bool_test = False
                                break

        return self.etotal


    def smooth_and_plot(self, criterion, k_x=None, x_label=None):
        '''
        Smooth the data and plot it.
        :param criterion: Margin to reach convergence.
               Note : energy units have to be Hartree.
        :param k_x: Since convergence studies made on kpt require variables such as ngkpt, or kptrlatt, it is not
               clear what to put on the x axis. You have to provide this
        :param k_x_label: What exactly represents k_x : 'k_z' ? inplane density ? uniform density ?
        :return: Data plotted for each parameter
        To do : add possibility to summarize all conv. studies in a single plot,
                add possibility to choose units.
        '''
        self.margin = criterion
        for i, key in enumerate(self.params):
            if key == 'ngkpt' or key == 'kptrlatt':
                if k_x == None or x_label==None:
                    raise ValueError('You need to provide what to put on the x-axis when making a convergence study'
                                     ' with kpts.')
                else:
                    pass
            else:
                pass
            #Smoothing
            if key == 'ngkpt' or key == 'kptrlatt':
                self.xaxis = k_x
                xlabel = x_label
            else:
                self.xaxis = self.params[key]
                xlabel = r'$\texttt{%s}$'+' (Ha)' %key
            #Model
            def model(x, A, u, b):
                return A * np.exp(-u * x) + b
            if key == 'pawecutdg':
                p0 = [-0.004,0.18,-223]
            else:
                p0 = [0.08,0.67,-223]
            [M, mu, r], pcov = curve_fit(model, self.xaxis, self.etotal[i], p0, method='trf')
            data_x = np.linspace(min(self.xaxis)-1, max(self.xaxis)+1, 100)
            data_y = model(data_x, M, mu, r)
            #Determine parameter
            k=0
            while np.abs(r-data_y[k])>self.margin:
                k+=1

            #Plotting
            axes = plt.gca()
            matplotlib.rcParams['figure.figsize'] = (20,15)
            matplotlib.rcParams['font.size'] = 15
            plt.plot(self.xaxis, [j/100 for j in self.etotal[i]], 'ro', markeredgecolor='k', markersize=3.5)
            plt.plot(data_x, [j/100 for j in data_y], ls='--', color='b', lw=1,
                     label=r'Lissage exponentiel')
            plt.axvline(x=data_x[k], ls=':')
            plt.xlabel(xlabel)
            plt.ylabel(r'$E_{\mathrm{total}}$ $\left(\mathrm{kHa} \right)$')
            #plt.legend(loc=4)
            axes.xaxis.set_tick_params(direction='in', length = 4)
            axes.yaxis.set_tick_params(direction='in', length = 4)

            plt.show()

            plt.clf()
            plt.cla()